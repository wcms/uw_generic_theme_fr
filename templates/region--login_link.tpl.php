<?php

/**
 * @file
 * Translate the login/out links from English to French.
 */

$content_fr = str_replace(array('Log in', 'Log out'), array('Connexion', 'Déconnexion'), $content);
print '<div id="cas_login">' . $content_fr . '</div>';
